
cd /some/non/existing/path

# echo "Success: $?"
# echo "LASTEXITCODE: $LASTEXITCODE"

if(!$?){
  Write-Output "Failed with code $LASTEXITCODE"
  # LASTEXITCODE only works for Win32 executables
  exit 1
}else{
  echo "Ok"
}
